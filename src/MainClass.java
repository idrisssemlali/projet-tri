import java.util.Scanner;

public class MainClass    {

    public static int[]Tab(int size, int max, int min){
        int[] tab = new int[10];

        for(int i = 0; i<tab.length; i++){
            tab[i]=(int)(Math.random() * (max+1-min)) + min;
        }
        return tab;
    }

    public static void Menu(){
        System.out.println("-------Menu------");
        System.out.println("1- Tri a bulle");
        System.out.println("2- Tri Rapide");
        System.out.println("3- Tri par fusion");
        System.out.println("Veuillez faire un choix !");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int Tableau[] = Tab(10, 9, 0);
        Sortable Stb = new TriBulle();
        Sortable Stf = new TriFusion();
        Sortable Str = new TriRapide();
        Menu();
        int choix = sc.nextInt();


        switch (choix) {
            case 1:
                Stb.sort(Tableau);
                for (int i = 0; i <= 9; i++)
                    System.out.print(Tableau[i]);
                System.out.println(" Tri a bulle effectué");
                break;

            case 2:
                Str.sort(Tableau);
                for (int i = 0; i <= 9; i++)
                    System.out.print(Tableau[i]);
                System.out.println(" Tri rapide effectué");
                break;

            case 3:
                Stf.sort(Tableau);
                for (int i = 0; i <= 9; i++)
                    System.out.print(Tableau[i]);
                System.out.println(" Tri par fusion effectué");
                break;
        }

    }
}
