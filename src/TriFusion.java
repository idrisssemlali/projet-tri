public class TriFusion implements  Sortable {

    @Override
    public void sort(int[] t) {

            int debut = 0;
            int fin = t.length-1;

        triFusion(t,debut,fin);

    }

    public  void triFusion(int t[])
    {
        int longeur=t.length;
        if (longeur>0)
        {
            triFusion(t,0,longeur-1);
        }
    }


    private  void triFusion(int t[],int debut,int fin)
    {
        if (debut!=fin)
        {
            int milieu=(fin+debut)/2;
            triFusion(t,debut,milieu);
            triFusion(t,milieu+1,fin);
            fusion(t,debut,milieu,fin);
        }
    }

    private  void fusion(int t[],int debut1,int fin1,int fin2)
    {
        int debut2=fin1+1;


        int table1[]=new int[fin1-debut1+1];
        for(int i=debut1;i<=fin1;i++)
        {
            table1[i-debut1]=t[i];
        }

        int compt1=debut1;
        int compt2=debut2;

        for(int i=debut1;i<=fin2;i++)
        {
            if (compt1==debut2)
            {
                break; //tous les éléments ont donc été classés
            }
            else if (compt2==(fin2+1)) //c'est que tous les éléments du second tableau ont été utilisés
            {
                t[i]=table1[compt1-debut1]; //on ajoute les éléments restants du premier tableau
                compt1++;
            }
            else if (table1[compt1-debut1]<t[compt2])
            {
                t[i]=table1[compt1-debut1]; //on ajoute un élément du premier tableau
                compt1++;
            }
            else
            {
                t[i]=t[compt2]; //on ajoute un élément du second tableau
                compt2++;
            }
        }
    }



}
