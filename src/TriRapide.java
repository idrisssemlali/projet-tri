public class TriRapide implements  Sortable {


    @Override
    public void sort(int[] t) {

         int debut= 0;
         int fin =t.length-1;

                triRapide(t , debut,fin);
    }

    public void triRapide(int [] tableau, int début, int fin) {
        if (début < fin) {
            int indicePivot = partition(tableau, début, fin);
            triRapide(tableau, début, indicePivot-1);
            triRapide(tableau, indicePivot+1, fin);
        }
    }

    public int partition (int []t, int début, int fin) {

        int valeurPivot = t[début];
        int d = début+1;
        int f = fin;

        while (d < f) {
            while(d < f && t[f] >= valeurPivot) {
                f--;
            };
            while(d < f && t[d] <= valeurPivot){
                d++;
            }

            int x = t[d];
            t[d]= t[f];
            t[f] = x;
        }
        if (t[d] > valeurPivot)
        {
            d--;

        }
        t[début] = t[d];
        t[d] = valeurPivot;
        return d;
    }

}
